# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-31 11:33+0430\n"
"PO-Revision-Date: 2020-10-21 20:30+0326\n"
"Last-Translator: b'  <>'\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Translated-Using: django-rosetta 0.8.1\n"

#: cart/forms.py:20 cart/templates/cart/detail.html:34
msgid "تعداد"
msgstr "تعداد"

#: cart/templates/cart/detail.html:7 cart/templates/cart/detail.html:17
msgid "سبد خرید"
msgstr "سبد خرید"

#: cart/templates/cart/detail.html:7 cart/templates/cart/detail.html:16
msgid "فروشگاه"
msgstr "فروشگاه"

#: cart/templates/cart/detail.html:15
msgid "خانه"
msgstr "خانه"

#: cart/templates/cart/detail.html:32
msgid "محصول"
msgstr "محصول"

#: cart/templates/cart/detail.html:33
msgid "قیمت"
msgstr "قیمت"

#: cart/templates/cart/detail.html:35
msgid "مجموع"
msgstr "مجموع"

#: cart/templates/cart/detail.html:57 cart/templates/cart/detail.html:68
#: cart/templates/cart/detail.html:137 cart/templates/cart/detail.html:143
#: cart/templates/cart/detail.html:151 cart/templates/cart/detail.html:159
#: cart/templates/cart/detail.html:215
msgid "ت"
msgstr "ت"

#: cart/templates/cart/detail.html:61
msgid "به روز رسانی سبد خرید"
msgstr "به روز رسانی سبد خرید"

#: cart/templates/cart/detail.html:129 cart/templates/cart/detail.html:141
msgid "جمع سبد خرید"
msgstr "جمع سبد خرید"

#: cart/templates/cart/detail.html:136
msgid "جمع کل سبد خرید"
msgstr "جمع کل سبد خرید"

#: cart/templates/cart/detail.html:147
msgid "کوپن"
msgstr "کوپن"

#: cart/templates/cart/detail.html:147
msgid "تخفیف"
msgstr "تخفیف"

#: cart/templates/cart/detail.html:156
msgid "جمع نهایی سبد خرید"
msgstr "جمع نهایی سبد خرید"

#: cart/templates/cart/detail.html:214
msgid "مبلغ قابل پرداخت"
msgstr "مبلغ قابل پرداخت"

#: cart/templates/cart/detail.html:222
msgid "رفتن به صفحه پرداخت"
msgstr "رفتن به صفحه پرداخت"

#: cart/templates/cart/detail.html:226
msgid "ادامه خرید"
msgstr "ادامه خرید"

#~ msgid "تومان"
#~ msgstr "تومان"

#~ msgid "Quantity"
#~ msgstr "تعداد"
